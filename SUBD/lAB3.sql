CREATE TABLE MOVIE
(
	ID INT IDENTITY(1,1) NOT NULL,
	NAME VARCHAR(20) NOT NULL,
	COUNTRY VARCHAR(20) NOT NULL,
	DATE_ DATETIME NOT NULL,
	PRICE REAL NOT NULL,
	DURATION DATETIME NOT NULL,
	CONSTRAINT MOVIE_ID PRIMARY KEY(ID)
);

CREATE TABLE GENRE
(
	ID INT IDENTITY(1,1) NOT NULL,
	NAME VARCHAR(20) NOT NULL,
	CONSTRAINT GENRE_ID PRIMARY KEY(ID)
);

CREATE TABLE PRODUCER
(
	ID INT IDENTITY(1,1) NOT NULL,
	NAME VARCHAR(20) NOT NULL,
	CONSTRAINT PRODUCER_ID PRIMARY KEY(ID)
);

CREATE TABLE HALL
(
	ID INT IDENTITY(1,1) NOT NULL,
	PLACE_COUNT INT NOT NULL,
	CONSTRAINT HALL_ID PRIMARY KEY(ID)	
);

CREATE TABLE USER_
(
	ID INT IDENTITY(1,1) NOT NULL,
	USERNAME VARCHAR(20) NOT NULL UNIQUE,
	PASSWORD VARCHAR(60) NOT NULL,
	LAST_NAME VARCHAR(20),
	FIRST_NAME VARCHAR(20),
	ACCESS_TYPE INT NOT NULL,
	ACCOUNT_STATUS INT NOT NULL,
	CONSTRAINT PK_USER_ID PRIMARY KEY(ID)
);

CREATE TABLE TICKET
(
	ID INT IDENTITY(1,1) NOT NULL,
	PRICE REAL NOT NULL,
	BUYING_TIME DATETIME NOT NULL,
	FK_SESSION_ID INT NOT NULL,
	FK_PLACE_ID INT NOT NULL,
	FK_USER_ID INT NOT NULL,
	FK_SUBSCRIPTION_ID INT,
	CONSTRAINT TICKET_ID PRIMARY KEY(ID)
);

CREATE TABLE ACTION
(
	ID INT IDENTITY(1,1) NOT NULL,
	DESCRIPTION VARCHAR(120),
	ACCESS_TYPE INT NOT NULL,
	CONSTRAINT ACTION_ID PRIMARY KEY(ID)
);

CREATE TABLE OBJECT_TYPE
(
	ID INT IDENTITY(1,1) NOT NULL,
	NAME VARCHAR(20) NOT NULL,
	CONSTRAINT OBJECT_TYPE_ID PRIMARY KEY(ID)
);

CREATE TABLE TIME_ALLOWANCE
(
	ID INT IDENTITY(1,1) NOT NULL,
	START_TIME DATETIME NOT NULL,
	END_TIME DATETIME NOT NULL,
	PRICE REAL NOT NULL,
	CONSTRAINT TIME_ALLOWANCE_ID PRIMARY KEY(ID)
);

CREATE TABLE PLACE
(
	ID INT IDENTITY(1,1) NOT NULL,
	PRICE REAL NOT NULL,
	ROW_ INT NOT NULL,
	PLACE INT NOT NULL,
	FK_HALL_ID INT NOT NULL,
	CONSTRAINT PLACE_ID PRIMARY KEY(ID),
	CONSTRAINT FK_PLACE_HALL_ID FOREIGN KEY(FK_HALL_ID) REFERENCES HALL(ID)
);

CREATE TABLE SUBSCRIPTION_TYPE
(
	ID INT IDENTITY(1,1) NOT NULL,
	SESSIONS_COUNT INT NOT NULL,
	PRICE REAL NOT NULL,
	IS_ACTIVE INT NOT NULL,
	CONSTRAINT SUBSCRIPTION_TYPE_ID PRIMARY KEY(ID)
);

CREATE TABLE SUBSCRIPTION
(
	ID INT IDENTITY(1,1) NOT NULL,
	PRICE REAL NOT NULL,
	BALANCE INT NOT NULL,
	FK_SUBSCRIPTION_TYPE_ID INT NOT NULL,
	FK_USER_ID INT NOT NULL,
	CONSTRAINT SUBSCRIPTION_ID PRIMARY KEY(ID),
	CONSTRAINT FK_SUBSCRIPTION_TYPE_ID FOREIGN KEY(FK_SUBSCRIPTION_TYPE_ID) REFERENCES SUBSCRIPTION_TYPE(ID),
	CONSTRAINT FK_SUBSCRIPTION_USER_ID FOREIGN KEY(FK_USER_ID) REFERENCES USER_(ID)
);

CREATE TABLE LOG_
(
	ID INT IDENTITY(1,1) NOT NULL,
	OLD_VALUE VARCHAR(120),
	DATE_ DATETIME NOT NULL,
	FK_USER_ID INT NOT NULL,
	FK_OBJECT_TYPE_ID INT NOT NULL,
	FK_ACTION_ID INT NOT NULL,
	CONSTRAINT LOG_ID PRIMARY KEY(ID),
	CONSTRAINT FK_LOG_USER_ID FOREIGN KEY(FK_USER_ID) REFERENCES USER_(ID),
	CONSTRAINT FK_OBJECT_TYPE_ID FOREIGN KEY(FK_OBJECT_TYPE_ID) REFERENCES OBJECT_TYPE(ID),
	CONSTRAINT FK_ACTION_ID FOREIGN KEY(FK_ACTION_ID) REFERENCES ACTION(ID)
);

CREATE TABLE SESSION_
(
	ID INT IDENTITY(1,1) NOT NULL,
	DATE_ DATETIME NOT NULL,
	FK_MOVIE_ID INT NOT NULL,
	FK_HALL_ID INT NOT NULL,
	FK_TIME_ALLOWANCE_ID INT NOT NULL,
	CONSTRAINT SESSION_ID PRIMARY KEY(ID),
	CONSTRAINT FK_SESSION_MOVIE_ID FOREIGN KEY(FK_MOVIE_ID) REFERENCES MOVIE(ID),
	CONSTRAINT FK_SESSION_HALL_ID FOREIGN KEY(FK_HALL_ID) REFERENCES HALL(ID),
	CONSTRAINT FK_TIME_ALLOWANCE_ID FOREIGN KEY(FK_TIME_ALLOWANCE_ID) REFERENCES TIME_ALLOWANCE(ID)
);

CREATE TABLE PRODUCER_MOVIE
(
	ID INT IDENTITY(1,1) NOT NULL,
	FK_MOVIE_ID INT NOT NULL,
	FK_PRODUCER_ID INT NOT NULL,
	CONSTRAINT PRODUCER_MOVIE_ID PRIMARY KEY(ID) ,
	FOREIGN KEY(FK_MOVIE_ID) REFERENCES MOVIE(ID),
	FOREIGN KEY(FK_PRODUCER_ID) REFERENCES PRODUCER(ID)
);

CREATE TABLE GENRE_MOVIE
(
	ID INT IDENTITY(1,1)
	 NOT NULL,
	FK_MOVIE_ID INT NOT NULL,
	FK_GENRE_ID INT NOT NULL,
	CONSTRAINT GENRE_MOVIE_ID PRIMARY KEY(ID),
	FOREIGN KEY(FK_MOVIE_ID) REFERENCES MOVIE(ID),
	CONSTRAINT FK_GENRE_ID FOREIGN KEY(FK_GENRE_ID) REFERENCES GENRE(ID)
);

ALTER TABLE TICKET ADD CONSTRAINT FK_TICKET_SESSION FOREIGN KEY(FK_SESSION_ID) REFERENCES SESSION_(ID);
ALTER TABLE TICKET ADD CONSTRAINT FK_TICKET_USER FOREIGN KEY(FK_USER_ID) REFERENCES USER_(ID);
ALTER TABLE TICKET ADD CONSTRAINT FK_TICKET_PLACE FOREIGN KEY(FK_PLACE_ID) REFERENCES PLACE(ID);
ALTER TABLE TICKET ADD CONSTRAINT FK_TICKET_SUBSCRIPTION FOREIGN KEY(FK_SUBSCRIPTION_ID) REFERENCES SUBSCRIPTION(ID);