create table Faculty
(
	ID int not null primary key PK_FACULTY,
	Name varchar(30) not null,
);

create table Subjects
(
	ID int not null primary key,
	Name varchar(120) not null,
	FACULTY_ID int constraint FK_FACULTY_SUBJECTS references Faculty(ID) 
);

create table Type_study_load
(
	ID numeric(10,0) not null primary key,
	Name varchar(120) not null
);

create table Study_load
(
	ID char(20) not null primary key,
	Hours int,
	TYPE_STUDY_ID numeric(10,0) not null constraint FK_TYPE_STUDY references Type_study_load(ID),
	SUBJECT_ID int not null constraint FK_SUBJECT_STUDY references Subjects(ID)
);

create table Students
(
	ID int not null primary key,
	Last_name varchar(60) not null,
	First_name varchar(30),
	GROUP_ID int
);

create table Groups
(
	ID int not null primary key,
	Name varchar(10) not null,
	HEAD_GROUP int constraint FK_HEAD_OF_GROUP references Students(ID),
	FACULTY_ID int not null constraint FK_FACULTY_GROUP references Faculty(ID)
);

alter table Students
add constraint FK_GROUP_OF_STUDENTS
foreign key (GROUP_ID) references Groups(ID);

create table Prepods
(
	ID int not null primary key,
	Last_name varchar(120) not null,
	First_name varchar(120),
	FACULTY_ID int not null foreign key references Faculty(ID)
);

create table Lessons
(
	GROUP_ID int not null constraint FK_GROUP_LESSONS references Groups(ID),
	PREPOD_ID int not null constraint FK_PREPOD_LESSONS references Prepods(ID),
	STUDY_ID char(20) not null constraint FK_STUDY_LESSONS references Study_load(ID),
	primary key(GROUP_ID, PREPOD_ID, STUDY_ID)
);

create table Rating
(
	ID int not null primary key,
	Date date not null,
	Val int,
	STUDENT_ID int not null constraint FK_STUDENT_RATING references Students(ID),
	STUDY_ID char(20) not null foreign key references Study_load(ID),
	PREPOD_ID int not null constraint FK_PREPOD_RATING references Prepods(ID),
	Is_absent char(1)
);



