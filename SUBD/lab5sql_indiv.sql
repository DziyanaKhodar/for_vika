select * from Prepods P, Faculty F where P.FACULTY_ID = F.ID;


select Last_name, First_name from Students
where GROUP_ID = (select min(ID) from Groups)
ORDER BY Last_name;


select * from Students 
where lower(Last_name) like '�%';

select Val from Rating R
inner join Prepods P on R.PREPOD_ID = P.ID
where (COALESCE(P.Last_name, '') + ' ' + COALESCE(P.First_name,'')) = '�������� '
and Val is not null
and YEAR(R.Date)=2016 and month(R.Date) = 10;


select COALESCE(S.Last_name, '') + ' ' + COALESCE(S.First_name,'')  from Rating R inner join Students S
on R.STUDENT_ID = S.ID
inner join Study_load SL on
R.STUDY_ID = SL.ID
inner join Type_study_load TSL
on SL.TYPE_STUDY_ID = TSL.ID
where TSL.ID = 1 
and R.Is_absent is not null;




select 
 COALESCE(S.Last_name, '') + ' ' + COALESCE(S.First_name,'') StudentFIO, 
COALESCE(P.Last_name, '') + ' ' + COALESCE(P.First_name,'') PrepodFIO,
 G.Name GroupName, R.Val Mark, R.Date MarkDate, FP.Name PrepodFaculty, SF.Name StudentFaculty, SubjF.Name SubjectFaculty 
 from Rating R 
inner join Students S
on R.STUDENT_ID = S.ID
inner join Prepods P on
P.ID = R.PREPOD_ID
inner join Faculty FP
on FP.ID = P.FACULTY_ID
inner join Groups G on
G.ID = S.GROUP_ID
inner join Faculty SF on
SF.ID = G.FACULTY_ID
inner join Study_load SL
on SL.ID = R.STUDY_ID
inner join Subjects Subj
on Subj.ID = SL.ID
inner join Faculty SubjF
on SubjF.ID = Subj.FACULTY_ID
where Val is not null;


select R.Date, TSL.Name type_study, Subj.Name subject, G.Name groupname from Rating R 
inner join Study_load SL 
on SL.ID = R.STUDY_ID
inner join Subjects Subj
on Subj.ID = SL.SUBJECT_ID
inner join Type_study_load TSL
on TSL.ID = SL.TYPE_STUDY_ID
inner join Lessons Les
on Les.STUDY_ID = SL.ID
inner join Groups G on
G.ID = Les.GROUP_ID
where Val is not null
and Les.PREPOD_ID != R.PREPOD_ID;

with prep_gr as (select distinct R.PREPOD_ID prepod, S.GROUP_ID groupid
 from Rating R
inner join Students S
on S.ID = R.STUDENT_ID
where R.Val is not null)

select G.Name grName, COALESCE(P.Last_name, '') + ' ' + COALESCE(P.First_name,'') PrepodFIO  from prep_gr
inner join Groups G
on G.ID = groupid
inner join Prepods P
on P.ID = prepod
order by P.Last_name;


select F.ID, F.NAME from Faculty F
where F.ID not in (SELECT distinct G.FACULTY_ID from Groups G);


