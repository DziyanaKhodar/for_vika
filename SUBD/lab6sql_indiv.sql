with Les as
(select S.GROUP_ID, R.STUDY_ID, R.PREPOD_ID, R.Date from Rating R
inner join Students S
on R.STUDENT_ID = S.ID
group by S.GROUP_ID, R.STUDY_ID, R.PREPOD_ID, R.Date)

select count(*) from Les;

select top 10 S.Last_name, S.First_name from
(select AVG(R.Val) av_mark, R.STUDENT_ID studentid from Rating R
where R.Val is not null
group by R.STUDENT_ID) MARKS
join Students S 
on MARKS.studentid = S.ID
ORDER BY MARKS.av_mark desc;


select count(distinct SL.SUBJECT_ID) subj_count, count(distinct TSL.ID) types_count from Rating R
inner join Study_load SL on
SL.ID = R.STUDY_ID
inner join Type_study_load TSL
on TSL.ID = SL.TYPE_STUDY_ID
where R.Val is not null;

select S.Last_name from Students S
where S.ID not in (select distinct R.STUDENT_ID from Rating R where R.Val is not null);

with avg_marks as (select avg(Val) avg_mark, STUDENT_ID s_id from Rating where Val is not null GROUP BY STUDENT_ID)

select S.* from avg_marks
inner join Students S
on S.ID = avg_marks.s_id
where avg_marks.avg_mark = (select max(avg_mark) from avg_marks);

